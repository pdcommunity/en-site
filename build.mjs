import Metalsmith  from 'metalsmith';
import markdown    from 'metalsmith-markdown';
import layouts     from 'metalsmith-layouts';
import permalinks  from 'metalsmith-permalinks';
import { rootFolder } from './paths.mjs';
import { articlePlugin } from './myPlugins/article.mjs';
import { indexWordPlugin } from './myPlugins/indexWord.mjs';

const rahnamoodText = `در این صفحه فهرست رهنمود های جمعیت داده های عمومی آمده است.  
رهنمود ها طبق [اساس نامه](../constitution) توسط هیئت تصمیم تعیین شده و
رعایت آن ها در فعالیت هایی که به نام جمعیت انجام می شود ضروری است.  

`;

export const build = async () => {
  return new Promise((res, rej) => {
    Metalsmith(rootFolder)
    .metadata({
      description: "It's about saying »Hello« to the World.",
      generator: "Metalsmith",
      url: "http://www.metalsmith.io/"
    })
    .source('./src')
    .destination('./public')
    .clean(false)
    .use(articlePlugin({
      path: 'articles',
      title: 'فهرست مقالات',
      text: 'در این صفحه نوشته های مرتبط با جمعیت داده های عمومی جمع آوری شده است.'+'\n\n',
    }))
    .use(articlePlugin({
      path: 'docs/guideline',
      title: 'فهرست رهنمود ها',
      text: rahnamoodText,
    }))
    .use(indexWordPlugin)
    .use(markdown())
    .use(permalinks())
    .use(layouts({
      engine: 'swig'
    }))
    .build(function(err, files) {
      if (err){
        rej(err);
      }
      else {
        res(files);
      }
    });
  });
}