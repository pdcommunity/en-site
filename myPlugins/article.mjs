export const articlePlugin = ({ text, title, path }) => (files, metalSmith, done) => {
  setImmediate(done);
  Object.keys(files).forEach((file) => {
    if (file.startsWith(path)) {
      const { title, incomplete } = files[file];
      if (!incomplete)
        text += `* [${title}](/${file.slice(0, -3)})\n`;
    }
  });
  text += '\n\n## پیش نویس ها\n\n';
  Object.keys(files).forEach((file) => {
    if (file.startsWith(path)) {
      const { title, incomplete } = files[file];
      if (incomplete)
        text += `* [${title}](/${file.slice(0, -3)})\n`;
    }
  });
  files[`${path}/index.md`] = {
    title,
    layout: 'article.swig',
    contents: new Buffer(text),
  };
};