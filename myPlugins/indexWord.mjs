const persianNum = (value) => {
  const t = value + "";
  let r = "";
  for (var i = 0; i < t.length; i++)
    r += String.fromCharCode(t.charCodeAt(i) + 1776 - 48);
  return r;
};

const replaceWithNum = (data, word) => {
  let cnt = 1;
  while (true) {
    if (!data.includes(word+'!')) return data.replace(/\#\*\+/g, word);
    data = data.replace(word+'!', '#*+ '+persianNum(cnt));
    cnt += 1;
  }
};

export const indexWordPlugin = (files, metalSmith, done) => {
  setImmediate(done);
  Object.keys(files).forEach((file) => {
    const { indexWords } = files[file];
    if (!indexWords) return;
    const s = files[file].contents.toString();
    let r = s;
    indexWords.forEach(k => {
      r = replaceWithNum(r, k);
    });
    files[file].contents = r;
  });
};