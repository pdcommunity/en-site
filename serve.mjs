import hs from "http-server";
import { buildFolder } from "./paths.mjs";

export const serve = async () => {
  const server = hs.createServer({
    root: buildFolder,
  });
  await new Promise((res)=>{
    server.listen(8080, '0.0.0.0', () => {
      res();
    });
  });
}