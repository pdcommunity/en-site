---
title: چه داده هایی واقعا عمومی هستند؟
layout: article.swig
indexWords: ['مثال']
---

ما داده های عمومی را به این صورت معرفی می کنیم که <<هر داده (محتوای قابل کپی و
قابل انتقال توسط کامپیوتر ها) ای که عموم جامعه مالک آن باشد و بتواند از آن به هر
طریقی استفاده کند داده عمومی است. >> اما هیچ چیز در دنیا به این سادگی نیست. در ادامه
به طور دقیق منظور از یک داده عمومی را بیان می کنیم و چند مثال نشان می دهیم که طبق
تعریف به نظر می رسد که عمومی باشند اما داده عمومی مد نظر ما نیستند.

## مثال!: نرم افزاری که بعد از مدتی از کار می افتد و باید خریداری شود

اگر چه می توان (به سختی) این داده را تغییر داد و آن قسمت مربوط به از کار افتادن را
از آن حذف کرد (کاری که به کرک کردن معروف است) اما
اگر این نرم افزار به این دلیل عمومی شمرده شود، می توان صندلی خانه شما را هم یک
صندلی در مالکیت عموم حساب کرد، زیرا هر کسی (به سختی) می تواند از پنجره خانه شما
وارد شود و آن را بردارد و استفاده کند.

## مثال!: کتابی که قوانین حق نشر اجازه کپی برداری از آن را نمی دهند

## مثال!: پوسته سمت کاربر یک سرویس
