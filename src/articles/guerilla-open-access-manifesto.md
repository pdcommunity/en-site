---
title: مانیفست نیروی دسترسی باز
layout: article.swig
incomplete: true
---

این مانیفست در سال ۲۰۰۸ توسط
[آرون شوارتز](https://fa.wikipedia.org/wiki/%D8%A2%D8%B1%D9%88%D9%86_%D8%B4%D9%88%D8%A7%D8%B1%D8%AA%D8%B2)
نوشته شده است. او هنگامی که چهار و نیم میلیون مقاله را که به وسیله دانشگاه
MIT
به آنها دسترسی داشت از سایت
JSTOR
دانلود کرد، توسط پلیس فدرال ایالات متحده امریکا دستگیر شد. او احتمالا
می خواست که مقالات دانلود شده را برای عموم درون اینترنت منتشر کند. او
قبل از دادگاهش (که احتمالا در آن به حدود ۳۵ سال زندان محکوم می شد) در سن
۲۶ سالگی خود کشی کرد. متن انگلیسی این مانیفست را می توانید از
[این لینک](https://archive.org/stream/GuerillaOpenAccessManifesto/Goamjuly2008_djvu.txt)
و ترجمه فارسی را در ادامه بخوانید.

## متن مانیفست

اطلاعات قدرت است. اما مثل همه قدرت ها، کسانی هستند که می خواهند آن را تنها برای
خودشان نگه دارند. تمام میراث عملی و فرهنگی جهان که در طول قرن ها در کتاب ها و مجلات منتشر
شده است، در حال دیجیتالی شدن و قفل شدن توسط شماری از شرکت های خصوصی است. می خواهید
مقالات علمی را بخوانید؟ پس باید هزینه هنگفتی را به ناشران مانند 
Reed Elsevier
پرداخت کنید.

کسانی در تلاشند تا این وضع را تغییر دهند. جنبش دسترسی باز با جدیت جنگیده است تا 
اطمینان حاصل کند که دانشمندان حق تکثیر خود را نمی فروشند و به جای آن اثرشان را در
اینترنت منتشر می کنند، تحت شرایطی که همه اجازه دسترسی داشته باشند. اما حتی در بهترین
حالات، کار آن‌ها تنها روی چیز هایی که در آینده منتشر می شوند اثر خواهد گذاشت. همه چیز
تا این لحظه از دست می رود.

این هزینه بیشتر از آن است که قابل پرداخت باشد. آکادمی ها را مجبور کنیم که برای خواندن
کار اعضایشان پول دهند؟ تمام کتابخانه شان را اسکن کنیم اما فقط به کارمندان گوگل اجازه دهیم تا
آن ها را بخوانند؟ مقالات علمی را در اختیار نخبه های دانشگاهی در جهان اول بگذاریم، اما نه
در اختیار کودکان در جنوب جهانی؟ این ظالمانه و غیر قابل قبول است.

بسیاری می گویند :« من موافقم. اما چه می توانیم بکنیم؟ آن شرکت ها حق تکثیر را در اختیار گرفته
اند و حجم عظیمی از پول را در ازای دسترسی بدست می آورند، و این کاملا قانونی است - هیچ
راهی برای جلوگیری از این کار نیست. » ولی چیزی هست که ما می توانیم، چیزی که هم اکنون در حال
انجام است. ما می توانیم متقابلا بجنگیم.

Those with access to these resources — students, librarians, scientists — you have been 
given a privilege. You get to feed at this banquet of knowledge while the rest of the world 
is locked out. But you need not — indeed, morally, you cannot — keep this privilege for 
yourselves. You have a duty to share it with the world. And you have: trading passwords 
with colleagues, filling download requests for friends. 



Meanwhile, those who have been locked out are not standing idly by. You have been 
sneaking through holes and climbing over fences, liberating the information locked up by 
the publishers and sharing them with your friends. 

But all of this action goes on in the dark, hidden underground. It's called stealing or 
piracy, as if sharing a wealth of knowledge were the moral equivalent of plundering a 
ship and murdering its crew. But sharing isn't immoral — it's a moral imperative. Only 
those blinded by greed would refuse to let a friend make a copy. 

Large corporations, of course, are blinded by greed. The laws under which they operate 
require it — their shareholders would revolt at anything less. And the politicians they 
have bought off back them, passing laws giving them the exclusive power to decide who 
can make copies. 

There is no justice in following unjust laws. It's time to come into the light and, in the 
grand tradition of civil disobedience, declare our opposition to this private theft of public 
culture. 

We need to take information, wherever it is stored, make our copies and share them with 
the world. We need to take stuff that's out of copyright and add it to the archive. We need 
to buy secret databases and put them on the Web. We need to download scientific 
journals and upload them to file sharing networks. We need to fight for Guerilla Open 
Access. 

با تعداد کافی از ما در سراسر جهان، نه تنها پیامی قوی علیه خصوصی سازی دانش 
فرستاده می شود، بلکه انحصار دانش به تاریخ می پیوندد. آیا شما به ما می پیوندید؟

آرون شوارتز 

جولای ۲۰۰۸، ارمو، ایتالیا