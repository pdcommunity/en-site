---
layout: faq.swig
title: سوالات متداول
---

This is list of Frequently Asked Questions.
If your question is not here,
[Ask us](/about#contact).

## Is public data a hidden harm to me that is provided to me for free?
خیر. برخلاف محصولات رایگان کمپانی ها که اغلب از مسیر های دیگری سود کسب می کنند
(مثل تبلیغات، جمع آوری اطلاعات، تاثیر روی فکر و ...)
داده های عمومی نمی توانند ضرری برای شما داشته باشند. زیرا هر کسی می تواند
آن ها را ببیند و قسمت های مضر آن را پاک کرده و دوباره منتشر کند. جامعه
مالک این داده هاست و کسی نمی تواند از آن سوء استفاده کند.

## چرا کسی باید داده هایش را برای عموم منتشر کند؟
علاوه بر نیت های خداپسندانه یا بشردوستانه، انگیزه های زیادی برای منتشر کردن
یک داده برای عموم وجود دارد. مثلا شما برای کار خود به یک ابزار نیاز دارید.
آن ابزار را تولید می کنید و در اختیار جامعه قرار می دهید. افراد زیادی از ابزار
شما استفاده می کنند و قسمتی که خودشان به آن نیاز دارند را توسعه می دهند.
به این ترتیب بعد از مدتی ابزار شما بسیار پیشرفت می کند بدون این که هزینه ای
برای آن کرده باشید.
[برای دیدن انگیزه های بیشتر این صفحه را ببینید](/articles/why-someone-make-common-data)
.

## داده های عمومی افراد را بیکار می کند و به صنایع آسیب می زند؟
ممکن است این طور به نظر برسد که داده های عمومی شرکت ها را از بین می برد و کارمندان
آن ها را بیکار می کند. اما در حقیقت شرکت هایی که از بین می روند شرکت های انحصاری
هستند که سودشان برای خودشان است و نه برای جامعه. اگر داده های عمومی با یک شرکت
انحصاری رقابت کرده و آن را نابود کنند، کارمندان آن شرکت می توانند به جای کمک
به انحصار و آسیب رساندن به جامعه، با توجه به تخصصشان در شرکت نابود شده،
به کار بر روی داده های عمومی جایگزین مشغول شوند.

## آیا این حق هر کسی نیست که داده هایش را انحصار کند؟
مفهوم حق مفهومیست که در طول تاریخ محل مناقشه بوده است. حق توسط قوانین یا مکاتب فکری مختلف به
چیز هایی متفاوت و بعضا متضاد اطلاق می شود؛ بنابراین نمی توان به طور مطلق در این باره
نظر داد.

می توان از دید سیستم های حقوقی مختلف بررسی کرد. مثلا اگر یک سیستم حقوقی بگوید کسی
حق احتکار ندارد، در آن صورت
[منطقی به نظر می رسد](/articles/why-hoarding-is-bad)
که حق انحصار داده را نیز به افراد ندهد.

اما جمعیت داده های عمومی به قوانین مختلف احترام می گذارد. ما هرگز تشویق نمی کنیم که
به افرادی که داده های خود را انحصار می کنند آسیب بزنید یا داده هایشان را به طور غیر قانونی
بدزدید. استراتژی های ما کاملا مسالمت آمیز است (مثلا از داده های انحصاری استفاده نکنید
تا انحصار کنندگان تنبیه شوند.) و حتی از قوانین و حقوقی که با داده های عمومی در تضاد
هستند به نفع داده های عمومی استفاده می کند. (مانند
[کپی لفت](/articles/what-is-copyleft)
)

## داده های عمومی چه ارتباطی با گرایش های چپ سیاسی دارند؟
[این صفحه را ببینید](/articles/public-data-vs-communism)
