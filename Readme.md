# pdcommunity site

## How to contribute

Contents are in the /src folder. Edit markdown files and make a merge request.

## build

Install nodejs latest version, then:

    $ npm install
    $ npm run build

Result will go to /public folder
