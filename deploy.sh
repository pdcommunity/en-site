cd $(dirname "$0")
host=$(jq -r '.host' < deploy-config.json)
SCRIPT="cd /pdcommunity/git; git pull; npm run build; cd ..; rm -r www; cp -a git/build/. www"
ssh $host "${SCRIPT}"
