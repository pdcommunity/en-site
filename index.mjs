import { build } from './build.mjs';
import { serve } from './serve.mjs';

const delay = (ms) => new Promise((res)=>setTimeout(res, ms));

const main = async () => {
  if (process.argv[2] === 'develop') {
    await serve();
    console.log('running at http://127.0.0.1:8080');
    console.log('rebuild every 2.0 second');
    while (true) {
      await build();
      await delay(2000);
    }
  }
  else if (process.argv[2] === 'build') {
    await build();
    console.log('OK');
  }
  else if (process.argv[2] === 'start') {
    await serve();
    console.log('running at http://127.0.0.1:8080');
  }
};

main();